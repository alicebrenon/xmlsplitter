{-# LANGUAGE NamedFieldPuns #-}
module Text.XML.Splitter (
      NodeCondition(..)
    , PathCondition(..)
    , XPath
    , split
    , xpath
  ) where

import Text.Parsec ((<|>), digit, letter, many, oneOf, spaces)
import Text.Parsec.String (Parser)
import Text.XML.Light (Content(..), Element(..), QName(..))

type DomElem = [Content]
data DomPath = DomPath {
      top :: Maybe (Element, DomPath)
    , left :: DomElem
    , right :: DomElem
  } deriving (Show)
data DomZipper = DomZipper {
      focus :: DomElem
    , path :: DomPath
  } deriving (Show)

data NodeCondition =
    Tag String
  | AttributesValues [(String, String)]

data PathCondition =
    Node NodeCondition

type XPath = [PathCondition]

tag :: Element -> String
tag = qName . elName

check :: PathCondition -> Element -> Bool
check (Node (Tag tagName)) element = tag element == tagName
check _ _ = False

open :: DomElem -> DomZipper
open dom = DomZipper {
      focus = dom
    , path = DomPath {
        top = Nothing
      , left = []
      , right = []
    }
  }

move :: DomZipper -> [PathCondition] -> Maybe DomZipper
move zipper [] = Just zipper
move (DomZipper {focus, path}) (condition:otherPathConditions) = do
  case break selectElem focus of
    (_, []) -> Nothing
    (left, (Elem newElem) : right) -> move (DomZipper {
          focus = elContent newElem
        , path = DomPath {
            top = Just (newElem { elContent = [] }, path)
          , left
          , right
        }
      }) otherPathConditions
    _ -> Nothing
  where
    selectElem (Elem element) = check condition element
    selectElem _ = False

zipUp :: DomZipper -> DomElem
zipUp (DomZipper {focus, path}) =
  case top path of
    Nothing -> focus
    Just (element, previousPath) -> zipUp $ DomZipper {
        focus = left path ++ Elem (element {elContent = focus}) : right path
      , path = previousPath
    }

pickOnlyOne :: (a -> Bool) -> [a] -> [[a]]
pickOnlyOne p l = pickOnlyOneAux [] l
  where
    pickOnlyOneAux _ [] = []
    pickOnlyOneAux stack (x:xs) =
      if p x
        then (reverse stack ++ (x:(filter (not . p) xs))) : pickOnlyOneAux stack xs
        else pickOnlyOneAux (x:stack) xs

split :: XPath -> [Content] -> [[Content]]
split [] dom = [dom]
split path dom =
  case move (open dom) (init path) of
    Nothing -> [dom]
    Just zipper@(DomZipper {focus}) ->
      let doms = pickOnlyOne p focus in
      fmap (zipUp . setFocus zipper) doms
  where
    p (Elem element) = check (last path) element
    p _ = False
    setFocus zipper focus = zipper { focus }

xpath :: Parser XPath
xpath = many $ do
  tagName <- (:) <$> (letter <|> oneOf "_:") <*> many (letter <|> digit <|> oneOf ".-_:")
  spaces
  return . Node $ Tag tagName
