(use-modules ((gnu packages haskell-xyz) #:select (ghc-xml))
             ((guix build-system haskell) #:select (haskell-build-system))
             ((guix git-download) #:select (git-predicate))
             ((guix gexp) #:select (local-file))
             ((guix licenses) #:select (bsd-3))
             ((guix packages) #:select (origin package)))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "ghc-xmlsplitter")
    (version "devel")
    (source
      (local-file %source-dir
          #:recursive? #t
          #:select? (git-predicate %source-dir)))
    (build-system haskell-build-system)
    (inputs (list ghc-xml))
    (home-page "https://gitlab.huma-num.fr/alicebrenon/xmlsplitter")
    (synopsis "A library to cut an XML file into several pieces")
    (description
      "XMLSplitter provides data types to represent XPaths and zipper-based
      functions to use them to separate sibling nodes and plug them back into
      the same DOM context.")
    (license bsd-3)))
